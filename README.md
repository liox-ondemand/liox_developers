[![Codeship](https://app.codeship.com/projects/73c63ef0-22b6-0135-eb40-52028c1190b7/status?branch=develop)](https://app.codeship.com/projects/221777)

# Lionbridge onDemand Developers Site

This project maintains the Developers web site at <http://developers.lionbridge.com>.

## Getting Started

To check out the project and install requirements do, the following:

```bash
$ git clone git@bitbucket.org:liox-ondemand/liox_developers.git
$ cd liox_developers
$ pipenv install --dev
```

## Local Development

To run a web server locally, do the following:

```bash
$ pipenv run -- fab server
```

Now navigate to <http://localhost:4567>.

## Deployment

[Codeship is configured](https://app.codeship.com/projects/221777) to deploy the following branches when they are pushed:

- `develop` -> <http://qa-developers.liondemand.com>
- `master` -> <http://developers.lionbridge.com>
