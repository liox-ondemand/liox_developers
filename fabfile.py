from fabric.api import task
from fabric.operations import local


@task(alias='s', default=True)
def server():
    """Start a local web server on port 4567"""
    local('python server.py')
