/*
Main Scripts
*/


var starter;

( function($) {"use strict";

starter = window.starter || {};
	

starter.owlCarousel = function (options){
	
	$.each(options, function(index,value) {
          
          if(value != null){
          	//if carousel has parameters 
         	$(index).owlCarousel(value);
          }else{
          	//if there are no parameters
          	$(index).owlCarousel();
          }
              
       });

		
};

// Section Scroll
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
               

// Documentation sidebar auto scroller
$(function() {
        
            if($(window).width() >= 1000){

              var $sidebar   = $(".sidebar-doc"), 
                $window    = $(window),
                offset     = $sidebar.offset(),
                topPadding = 30;

            $window.scroll(function() {
                if ($window.scrollTop() > offset.top) {
                    $sidebar.stop().animate({
                        marginTop: $window.scrollTop() - offset.top + topPadding
                    });
                } else {
                    $sidebar.stop().animate({
                        marginTop: 0
                    });
                }
            });

            }
    
});               


starter.bootstrapCarousel = function (options){
	
		$.each(options, function(index,value) {
          
          if(value != null){
          	//if carousel has parameters 
         	$(index).carousel(value);
          }else{
          	//if there are no parameters
          	$(index).carousel();
          }
              
       });

};

starter.fitVids = function (selector){	
	$(selector).fitVids();	      
};

starter.scrollAnim = function(option){	
		$(window).load(function() {
			if(option == "yes"){		
					//trigger css3 animations
					// Handle appear event for animated elements
					var wpOffset = 80;
					if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
						wpOffset = 100;

						$.fn.waypoint.defaults = {
							context : window,
							continuous : true,
							enabled : true,
							horizontal : false,
							offset : 0,
							triggerOnce : false
						};

						$('.animated').waypoint(function() {
							var elem = $(this);
							var animation = elem.data('animation');
							if (!elem.hasClass('visible') && elem.attr('data-animation') !== undefined) {
								if (elem.attr('data-animation-delay') !== undefined) {
									var timeout = elem.data('animation-delay');
									setTimeout(function() {
										elem.addClass(animation + " visible");
									}, timeout);
								} else {
									elem.addClass(elem.data('animation') + " visible");
								}
							}
						}, {
							offset : wpOffset + '%'
						});
					} else {
						//if mobile, don't do it just display elements
						$('.animated').each(function() {
							$(this).css("visibility", "visible");
						});
					}



				}else{
				//don't trigger css3 animation, but display elements
						$('.animated').each(function() {
							$(this).css("visibility", "visible");
						});
				}
		}); //window load
};

if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {

		$(".background-middle-full").removeClass("fixed");
};


}(jQuery)); 


$(window).resize(function() {
    $('.head-root').height($(document).height() - 35);
});

$(window).trigger('resize');

    $(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).show();
                $(this).toggleClass('open');
                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).hide();
                $(this).toggleClass('open');
              
            });
    });

                
// Mobile nav propagate links
$("ul.navbar-nav").clone().appendTo(".pushy-content");
$('.pushy-content ul, .pushy-content li').each(function() {
            var attributes = this.attributes;
            var i = attributes.length;
            while( i-- ){
                this.removeAttributeNode(attributes[i]);
            }
});
                
$('.pushy-content a#drop1').hide();

