#! /usr/bin/env python

# Adapted from http://louistiao.me/posts/python-simplehttpserver-recipe-serve-specific-directory/

import posixpath
import urllib
import os

from SimpleHTTPServer import SimpleHTTPRequestHandler
from BaseHTTPServer import HTTPServer

class RootedHTTPServer(HTTPServer):
    def __init__(self, base_path, *args, **kwargs):
        HTTPServer.__init__(self, *args, **kwargs)
        self.RequestHandlerClass.base_path = base_path

class RootedHTTPRequestHandler(SimpleHTTPRequestHandler):
    def translate_path(self, path):
        path = path.split('?')[0]
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = self.base_path
        for word in words:
            word = os.path.splitdrive(word)[1]
            word = os.path.split(word)[1]
            if word in (os.curdir, os.pardir):
                continue
            path = os.path.join(path, word)
        return path

def serve(handler_class=RootedHTTPRequestHandler, server_class=RootedHTTPServer):
    server_address = ('', 4567)

    httpd = server_class("source", server_address, handler_class)

    address = httpd.socket.getsockname()
    print "Serving HTTP on", address[0], "port", address[1], "..."
    httpd.serve_forever()

if __name__ == '__main__':
    serve()
